# File Splitter
Il progetto, realizzato per un corso d'insegnamento universitario sulla programmazione ad oggetti, sviluppa un'applicazione Java con le seguenti funzionalità:
- creazione di una coda di file da dividere, ognuno con il proprio metodo di divisione; 
- esecuzione della divisione dei file in coda; 
- ricomposizione delle parti di un file. 

I metodi di divisione disponibili sono:
- divisione in più parti specificando la dimensione di ogni parte;
- divisione in più parti specificando la dimensione di ogni parte e crittografando il contenuto dei file generati tramite una chiave;
- divisione in più parti specificando la dimensione di ogni parte e comprimendo il contenuto dei file generati;
- divisione in più parti specificando il numero di parti.


### Strumenti e risorse utilizzate
L'applicazione è stata realizzata usando il linguaggio Java. Essa è inoltre accompagnata da pagine di documentazione HTML, generate tramite Javadoc, che descrivono le scelte di progetto effettuate e la struttura del software.


### Istruzioni per l'installazione
Per usufruire del programma, è sufficiente scaricare l'eseguibile [filesplitter.jar](filesplitter.jar) e avviarlo.