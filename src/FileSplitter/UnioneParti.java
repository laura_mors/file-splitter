package FileSplitter;

import java.io.*;

import javax.swing.JOptionPane;

/**
 * Classe che descrive una modalit� di unione dei file in cui le varie parti di un file,
 * precedentemente diviso, vengono unite per ricostruire il file originale.
 * <p>
 * Eredita da {@link Unione}.
 * 
 * @author Laura Morselli
 *
 */
public class UnioneParti extends Unione {
	
	/**
	 * L'array utilizzato per leggere da un file.
	 */
	private byte[] inputBytes;

	/**
	 * Crea una nuova istanza di {@link UnioneParti} con il file specificato, costituente
	 * la prima parte del file da ricostruire.
	 * 
	 * @param file il file da cui verr� avviata l'unione
	 */
	public UnioneParti(File file) {
		
		super(file);
	}

	/**
	 * Metodo che unisce le varie parti di un file precedentemente diviso, ottenendo cos� il file originale. La 
	 * ricomposizione verr� avviata dal file fornito quando si � creata un'istanza di {@link UnioneParti}.
	 * <p>
	 * Utilizza un {@code FileInputStream} per leggere i vari file, ed un {@code FileOutputStream} per scrivere
	 * ed unire il loro contenuto in un unico file.
	 */
	@Override
	public void unisci() {
		
		try {
			
			File fileCompleto = CreazioneFile.creaNuovoFileUnione(super.getFile());
			
			FileOutputStream fos = new FileOutputStream(fileCompleto, true);
			
			int bytesLetti = 0;
			
			for (int i = 0; i < super.getNumPartiFile(); i++) {	
				
				FileInputStream fis = new FileInputStream(super.getPartiFile()[i]);
				
				inputBytes = new byte[(int) super.getPartiFile()[i].length()];
				
				while ( ( ( bytesLetti = fis.read(inputBytes) ) != -1 ) && 
						bytesLetti != 0 ) {
					
					fos.write(inputBytes, 0, bytesLetti);
				}
				
				fis.close();
			}
			
			fos.close();
			
			Unione.mostraMessaggioCompletamento(fileCompleto.getName());
		}
		catch(IOException e) {
			
			JOptionPane.showMessageDialog(null, 
				"<html><body><p style='width: 500px;'>Si � verificato un errore durante " + 
				"l'unione del file " + super.getFile().getName() + ".", 
				"Errore", JOptionPane.ERROR_MESSAGE);
		}
		
	}

}
