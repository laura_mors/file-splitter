package FileSplitter;

import java.io.*;

import javax.swing.JOptionPane;

/**
 * Classe che descrive una modalit� di divisione dei file basata sul dividerli in pi� parti, 
 * ognuna memorizzata in un file diverso.
 * <p>
 * Eredita da {@link Divisione}.
 * <p>
 * Implementa le interfacce {@link DivisionePerDim} e {@link DivisionePerParti}.
 * 
 * @author Laura Morselli
 *
 */
public class DivisioneParti extends Divisione implements DivisionePerDim, DivisionePerParti {
	
	/**
	 * Costante che descrive la dimensione dell'array utilizzato per leggere da un file.
	 */
	private final int INPUT_ARR_DIM = 8*1024;
	
	/**
	 * Carattere utilizzato per riconoscere il tipo di divisione da utilizzare 
	 * ({@link tipoDivisione#DIM_PARTI DIM_PARTI} oppure 
	 * {@link tipoDivisione#NUM_PARTI NUM_PARTI}).
	 */
	private char tipoDiv;
	/**
	 * L'array utilizzato per leggere il contenuto di un file.
	 */
	private byte[] inputBytes = new byte[INPUT_ARR_DIM];
	
	/**
	 * Crea una nuova istanza di {@link DivisioneParti} con il {@link FileScelto} e la dimensione 
	 * della singola parte specificati; quest'ultima � poi usata per calcolare il numero di parti
	 * in cui verr� diviso il file.
	 * <p>
	 * L'intestazione da apporre all'inizio dei nomi dei file risultanti � impostata a quella del
	 * tipo di divisione {@link tipoDivisione#DIM_PARTI DIM_PARTI}.
	 * 
	 * @param file il {@link FileScelto} su cui verr� attuata la divisione
	 * @param dimParte la dimensione che ognuno dei file generati dalla divisione dovr� avere
	 */
	public DivisioneParti(FileScelto file, double dimParte) {
		
		super(file);
		super.setDimParte(dimParte);
		super.setNumParti(calcolaNumParti(dimParte));
		this.tipoDiv = tipoDivisione.DIM_PARTI.getIntest();
	}
	
	/**
	 * Crea una nuova istanza di {@link DivisioneParti} con il {@link FileScelto} e il numero di parti
	 * specificato; quest'ultimo � poi usato per calcolare la dimensione che ognuno dei file generati
	 * dalla divisione dovr� avere. 
	 * <p>
	 * L'intestazione da apporre all'inizio dei nomi dei file risultanti � impostata a quella del
	 * tipo di divisione {@link tipoDivisione#NUM_PARTI NUM_PARTI}.
	 * 
	 * @param file il {@link FileScelto} su cui verr� attuata la divisione
	 * @param numParti il numero di parti in cui verr� diviso il file
	 */
	public DivisioneParti(FileScelto file, int numParti) {
		
		super(file);
		super.setNumParti(numParti);
		super.setDimParte(calcolaDimParte(numParti));
		this.tipoDiv = tipoDivisione.NUM_PARTI.getIntest();
	}

	/**
	 * Metodo che attua la divisione di un file in pi� parti. Il numero di file generati dipende dalla dimensione 
	 * o dal numero di parti forniti quando si � creata un'istanza di {@link DivisioneParti}.
	 * <p>
	 * Utilizza un {@code FileInputStream} per leggere dal file originario, ed un {@code FileOutputStream}
	 * per scrivere sui file generati.
	 */
	@Override
	public void dividi() {
		
		try {
			
			File nuovaCartella = CreazioneFile.creaNuovaCartella(super.getFile(), tipoDiv);
			
			FileInputStream fis = new FileInputStream(super.getFile());
			
			int bytesLetti = 0;
			
			for (int i = 0; i < super.getNumParti(); i++) {
			
				File nuovoFile = CreazioneFile.creaNuovoFileDiv(nuovaCartella, 
						super.getFile().getName(), tipoDiv, i+1);
			
				FileOutputStream fos = new FileOutputStream(nuovoFile, true);
			
				if ( bytesLetti != -1 && bytesLetti != 0 ) {
					
					fos.write(inputBytes, 0, bytesLetti);
				}
			
				while ( ( ( bytesLetti = fis.read(inputBytes) ) != -1 ) && 
						( bytesLetti != 0) &&
						( ( nuovoFile.length()) <= super.getDimParte() ) ) {
				
					fos.write(inputBytes, 0, bytesLetti);
					
					bytesLetti = 0;
				}
			
				fos.close();
			}
		
			fis.close();
		}
		catch (IOException e) {
			
			JOptionPane.showMessageDialog(null, 
				"<html><body><p style='width: 500px;'>Si � verificato un errore durante " + 
				"la divisione in parti del file " + super.getFile().getName() + ".", 
				"Errore", JOptionPane.ERROR_MESSAGE);
		}
		
	}
	
	/**
	 * Metodo che restituisce la costante {@link #INPUT_ARR_DIM}.
	 * @return la costante {@link #INPUT_ARR_DIM}
	 */
	public int getInputArrDim() {
		
		return INPUT_ARR_DIM;
	}
	
	/**
	 * Metodo che calcola il numero di parti in cui sar� diviso il file scelto per la divisione, 
	 * dividendo la dimensione del suddetto file per {@code dimParte} e arrotondando il risultato. 
	 * <p>
	 * Nel caso in cui il numero cos� ottenuto sia stato arrotondato per difetto, esso viene aumentato di {@code 1}
	 * prima di essere restituito, in modo da avere abbastanza parti per contenere tutto il file originale.
	 */
	@Override
	public int calcolaNumParti(double dimParte) {
		
		int num = (int) Math.round( ( super.getDimFile()  / dimParte ) );
		
		if ( ( ( super.getDimFile()  / dimParte ) > num ) ) {
			
			num += 1;
		}
		
		return num;
	}
	
	/**
	 * Metodo che calcola la dimensione che ognuno dei file generati dalla divisione dovr� avere, dividendo
	 * la dimensione del file originale per {@code numParti}.
	 */
	@Override
	public double calcolaDimParte(int numParti) {
		
		return (super.getDimFile() / numParti);
	}
	

}
