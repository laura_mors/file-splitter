package FileSplitter;

import java.awt.*;
import java.awt.event.*;
import javax.swing.*;
import javax.swing.event.*;
import java.util.List;
import java.io.*;

import static java.awt.GridBagConstraints.*;
import static FileSplitter.Divisione.tipoDivisione.*;

/**
 * Classe utilizzata per la creazione del pannello descrivente l'interfaccia grafica dell'applicazione.
 * 
 * @author Laura Morselli
 *
 */
@SuppressWarnings("serial")
public class PannelloPrincipale extends JPanel implements ActionListener {

	/**
	 * Classe che attua la divisione di un file in un thread in background.
	 * 
	 * @author Laura Morselli
	 *
	 */
	private class EsecuzioneDiv extends SwingWorker<Void, Void> {
		
		/**
		 * L'istanza di {@link Divisione} da essere eseguita in un thread in background.
		 */
		private Divisione divisione;
		
		/**
		 * Crea una nuova istanza di {@link EsecuzioneDiv} con l'istanza di {@link Divisione} specificata.
		 * 
		 * @param divisione l'istanza di {@link Divisione} da eseguire in un thread in background
		 */
		public EsecuzioneDiv (Divisione divisione) {
			
			this.divisione = divisione;
		}

		/**
		 * Metodo che esegue la divisione di un file in un thread in background, invocando il metodo
		 * {@link Divisione#dividi() dividi()} della {@link #divisione} associata a questa istanza di
		 * {@link EsecuzioneDiv}.
		 */
		@Override
		protected Void doInBackground()  {
			
			divisione.dividi();
			
			return null;
		}
		
		/**
		 * A divisione del file completata, questo metodo rimuove il {@link FileScelto} corrispondente
		 * dalla {@link PannelloPrincipale#listaFile lista} presente nell'interfaccia e dalla 
		 * {@link PannelloPrincipale#codaFile coda dei file}, e aggiorna l'{@link PannelloPrincipale#jpb avanzamento}
		 * della situazione globale di divisione.
		 */
		public void done() {
			
			listModel.remove(0);
			codaFile.rimuoviTesta();
			
			int totFile = contNumFile;
			int numFileDivisi = totFile - codaFile.getDim();
			
			jpb.setString(numFileDivisi + " / " + totFile);
			
			if (numFileDivisi == totFile) {
				
				contNumFile = 0;
				avviaDiv.setEnabled(false);
			}
          
        }	
		
	}
	
	/**
	 * Costante che rappresenta il massimo numero di file selezionabili per la divisione.
	 */
	private final int MAX_NUM_FILE = 50;
	/**
	 * Costante che rappresenta 1024 byte, utilizzata per convertire le dimensioni dei file in un formato
	 * meglio comprensibile all'utente, e per convertire le dimensioni scelte dall'utente in byte.
	 */
	private final int CONVERT_BYTE = 1024;
	
	/**
	 * Il {@code GridBagLayout} utilizzato.
	 */
	GridBagLayout layout;
	/**
	 * Il {@code GridBagConstraints} utilizzato per {@link #listaFile}.
	 */
	GridBagConstraints limLista;
	/**
	 * Il {@code GridBagConstraints} utilizzato per {@link #listScroller}.
	 */
	GridBagConstraints limScroller;
	/**
	 * Il {@code GridBagConstraints} utilizzato per {@link #spiega}, {@link #scegli}, {@link #modifica},
	 * {@link #elimina}, {@link #scegliDiv}, {@link #avviaDiv}, {@link #jpb}, e {@link #unisci}.
	 */
	GridBagConstraints lim;
	/**
	 * Il modello su cui si basa {@link #listaFile}.
	 */
	private DefaultListModel<FileScelto> listModel;
	/**
	 * La lista utilizzata per mostrare sull'interfaccia grafica i file scelti dall'utente.
	 */
	private JList<FileScelto> listaFile;
	/**
	 * La barra di scorrimento di {@link #listaFile}.
	 */
	private JScrollPane listScroller;
	/**
	 * Area utilizzata per visualizzare una breve spiegazione dello scopo del programma.
	 */
	private JLabel spiega;
	/**
	 * Il pulsante utilizzato per permettere all'utente di scegliere dei file.
	 */
	private JButton scegli;
	/**
	 * Il pulsante utilizzato per permettere all'utente di modificare dei file fra quelli selezionati.
	 */
	private JButton modifica;
	/**
	 * Il pulsante utilizzato per permettere all'utente di eliminare dei file fra quelli selezionati.
	 */
	private JButton elimina;
	/**
	 * Il pulsante utilizzato per permettere all'utente di scegliere il tipo di divisione di uno o pi� file.
	 */
	private JButton scegliDiv;
	/**
	 * Il pulsante utilizzato per permettere all'utente di avviare la divisione dei file.
	 */
	private JButton avviaDiv;
	/**
	 * Barra di avanzamento utilizzata per mostrare il progresso dell'operazione globale di divisione.
	 */
	private JProgressBar jpb;
	/**
	 * Il pulsante utilizzato per permettere all'utente di unire dei file.
	 */
	private JButton unisci;
	/**
	 * Istanza di {@link SelezioneFile} utilizzata per eventi che permettono la selezione multipla.
	 */
	private SelezioneFile selezMulti = new SelezioneFile();
	/**
	 * Istanza di {@link SelezioneFile} utilizzata per eventi che prevedono solo la selezione singola.
	 */
	private SelezioneFile selezSingola  = new SelezioneFile();
	/**
	 * Array contenente i file da aggiungere scelti dall'utente.
	 */
	private File[] files = new File[MAX_NUM_FILE];
	/**
	 * Contatore che tiene traccia del numero di file selezionati dall'utente e presenti nella 
	 * {@link #codaFile coda}.
	 */
	private int contNumFile = 0;
	/**
	 * Istanza di {@link CodaFile} che rappresenta la coda di file da dividere.
	 */
	private CodaFile<FileScelto> codaFile = new CodaFile<FileScelto>();
	/**
	 * Lista utilizzata per contenere i file che l'utente seleziona dalla {@link #listaFile}.
	 */
	private List<FileScelto> fileSelez;
	/**
	 * Array utilizzato per contenere gli indici dei file che l'utente seleziona dalla {@link #listaFile}.
	 */
	private int[] fileSelezIndex;
	/**
	 * Array per contenere delle istanze di {@link Divisione}.
	 */
	private Divisione divisioni[];
	
	/**
	 * Crea un'istanza di {@link PannelloPrincipale}.
	 */
	public PannelloPrincipale () {
		
		super();
		layout = new GridBagLayout();
		limLista = new GridBagConstraints();
		limScroller = new GridBagConstraints();
		lim = new GridBagConstraints();
		this.setLayout(layout);
		
		listModel = new DefaultListModel<FileScelto>();
		
		listaFile = new JList<FileScelto>(listModel);
		listaFile.setPreferredSize(new Dimension(600, 900));
		listaFile.setMinimumSize(new Dimension(250, 350));
		listaFile.setSelectionMode(ListSelectionModel.MULTIPLE_INTERVAL_SELECTION);
		/*
		 * impostato a -1 in quanto permette alla lista di mostrare il massimo numero di oggetti possibili
		 * nello spazio disponibile su schermo
		 */
		listaFile.setVisibleRowCount(-1);
		listaFile.setSelectionBackground(new Color(167, 233, 191));
		listaFile.addListSelectionListener(new ListSelectionListener() {
			
			public void valueChanged(ListSelectionEvent evt) {
				
				if (evt.getValueIsAdjusting() == false) {
					
					if (listaFile.getSelectedIndex() == -1) {
						
						modifica.setEnabled(false);
						elimina.setEnabled(false);
						scegliDiv.setEnabled(false);
					}
					else {
						
						modifica.setEnabled(true);
						elimina.setEnabled(true);
						scegliDiv.setEnabled(true);
					}
				}
			}
		});

		setLim(limLista, 0, 0, 1, 4, 1, 0, BOTH);
		this.add(listaFile, limLista);
		
		listScroller = new JScrollPane(listaFile);
		listScroller.setPreferredSize(new Dimension(400,600));
		listScroller.setMinimumSize(new Dimension(200, 300));
		setLim(limScroller, 1, 0, 1, 4, 0, 0, VERTICAL);
		this.add(listScroller,limScroller);	
		
		spiega = new JLabel("Scegli dei file da dividere, oppure un file da ricomporre.");
		setLim(lim, 2, 0, 3, 1, 0, 1, NONE);
		this.add(spiega, lim);
		
		scegli = new JButton("Scegli");
		setLim(lim, 2, 1, 1, 1, 1, 1, NONE);
		scegli.addActionListener(this);
		this.add(scegli, lim);
		
		modifica = new JButton("Modifica");
		modifica.setEnabled(false);
		setLim(lim, 3, 1, 1, 1, 1, 1, NONE);
		modifica.addActionListener(this);
		this.add(modifica, lim);
		
		elimina = new JButton("Elimina");
		elimina.setEnabled(false);
		setLim(lim, 4, 1, 1, 1, 1, 1, NONE);
		elimina.addActionListener(this);
		this.add(elimina, lim);
		
		scegliDiv = new JButton("Cambia tipo di divisione");
		scegliDiv.setEnabled(false);
		setLim(lim, 2, 2, 1, 1, 1, 1, NONE);
		scegliDiv.addActionListener(this);
		this.add(scegliDiv, lim);
		
		avviaDiv = new JButton("Avvia divisione");
		avviaDiv.setEnabled(false);
		setLim(lim, 3, 2, 1, 1, 1, 1, NONE);
		avviaDiv.addActionListener(this);
		this.add(avviaDiv, lim);
		
		jpb = new JProgressBar();
		jpb.setStringPainted(true);
		jpb.setString("- / -");
		setLim(lim, 4, 2, 1, 1, 1, 1, NONE);
		this.add(jpb, lim);
		
		unisci = new JButton("Unisci dei file");
		setLim(lim, 2, 3, 3, 1, 1, 1, NONE);
		unisci.addActionListener(this);
		this.add(unisci, lim);
	}

	/**
	 * Metodo contenente l'implementazione delle azioni che devono verificarsi quando uno fra i pulsanti 
	 * {@link #scegli}, {@link #modifica}, {@link #elimina}, {@link #scegliDiv}, {@link #avviaDiv} o {@link #unisci}
	 * � premuto.
	 */
	@Override
	public void actionPerformed(ActionEvent e) {
		
		Object pulsantePremuto = e.getSource();
		
		if (pulsantePremuto == scegli) {
			
			if (selezMulti.selezioneMultipla()) {
				
				files = selezMulti.getFileSelezionati();
				
				boolean limiteFileRaggiunto = false;
				
				for (int i = 0; i < files.length && !limiteFileRaggiunto; i++) {
					
					if (contNumFile >= MAX_NUM_FILE) {
						
						limiteFileRaggiunto = true;
					}
					else {
						
						contNumFile++;
						FileScelto fileScelto = new FileScelto(contNumFile, files[i]);
						listModel.addElement(fileScelto);
						codaFile.aggiungi(fileScelto);
					}
				}
				
				if (limiteFileRaggiunto) {
					
					JOptionPane.showMessageDialog(this, 
							"<html><body><p style='width: 500px;'>Raggiunto il limite di file " +
							"selezionabili (" + MAX_NUM_FILE + ").</p></body></html>", 
							"Errore", JOptionPane.ERROR_MESSAGE);
				}
				
				avviaDiv.setEnabled(true);
			}
			
		}
		
		if (pulsantePremuto == modifica) {
			
			fileSelez = listaFile.getSelectedValuesList();
			fileSelezIndex = listaFile.getSelectedIndices();
			
			if (selezMulti.selezioneMultipla()) {
				
				files = selezMulti.getFileSelezionati();
				
				if (files.length == fileSelez.size() ) {
					
					for (int i = 0; i < fileSelez.size(); i++) {
						
						listModel.removeElement(fileSelez.get(i));
						codaFile.rimuovi(fileSelez.get(i));
						//aumento di 1 il contenuto di fileSelezIndex perch� gli indici della lista partono da 0
						FileScelto fileScelto = new FileScelto(fileSelezIndex[i]+1, files[i]); 
						listModel.add(fileSelezIndex[i], fileScelto);
						codaFile.aggiungi(fileSelezIndex[i], fileScelto);
					}	
				}
				else {
					
					JOptionPane.showMessageDialog(this, 
							"<html><body><p style='width: 500px;'>Il numero di file da modificare ed " +
							"il numero di file con cui li si vuole scambiare devono coincidere.</p></body></html>", 
							"Errore", JOptionPane.ERROR_MESSAGE);
				}
			}
		}
		
		if (pulsantePremuto == elimina) {
			
			fileSelez = listaFile.getSelectedValuesList();
			
			for (int i = 0; i < fileSelez.size(); i++) {
				
				contNumFile--;
				listModel.removeElement(fileSelez.get(i));
				codaFile.rimuovi(fileSelez.get(i));
			}
			
			if (contNumFile == 0) {
				
				avviaDiv.setEnabled(false);
			}
		}
		
		if (pulsantePremuto == scegliDiv) {

			fileSelezIndex = listaFile.getSelectedIndices();
			
			String[] scelte = {DIM_PARTI.getDescDiv(), CRITTOGRAFIA.getDescDiv(),
							   COMPRESSIONE.getDescDiv(), NUM_PARTI.getDescDiv()};
			
			String sceltaSelez = (String) JOptionPane.showInputDialog(this, "Scegli un tipo", 
								 "Tipi di divisione", JOptionPane.INFORMATION_MESSAGE, null, 
								  scelte, scelte[0]);
			
			if (sceltaSelez != null ) {
			
				for (int i = 0; i < fileSelezIndex.length; i++) {
					
					codaFile.getElem(fileSelezIndex[i]).setTipoDivisione(sceltaSelez);
					this.repaint();
				}
			}	
			
		}
		
		if (pulsantePremuto == avviaDiv) {
			
			divisioni = new Divisione[contNumFile];
			
			boolean procediDiv = true;
			
			for (int i = 0; i < contNumFile && procediDiv; i++) {
				
				String controlloDiv = codaFile.getElem(i).getTipoDivisione();
				
				String sceltaParte = "";
				boolean dimensione;
				boolean parteValida = false;
				
				while (!parteValida && procediDiv) {
					
					if(Divisione.getIdDivisione(controlloDiv) != NUM_PARTI.ordinal()) {
						
						sceltaParte = (String) JOptionPane.showInputDialog(this, 
									"<html><body><p style='width: 500px;'>Inserisci quanto vuoi che " + 
									"sia grande la singola parte del file \"" +
									codaFile.getElem(i).getFile().getName() + 
									"\" (dimensione file: " + codaFile.getElem(i).getDescDimFile() + " ), " +
									"assieme ad una unit� di grandezza (kb, mb, gb).</p></body></html>", 
									"Dimensione parte", JOptionPane.INFORMATION_MESSAGE, null, 
									null, "");
						
						dimensione = true;
						
						}
						else {
							
							sceltaParte = (String) JOptionPane.showInputDialog(this, 
									"<html><body><p style='width: 500px;'>Inserisci il numero di parti in " + 
									"cui vuoi dividere il file \"" +
									codaFile.getElem(i).getFile().getName() + 
									"\" (dimensione file: " + codaFile.getElem(i).getDescDimFile() + 
									").</p></body></html>", 
									"Numero parti", JOptionPane.INFORMATION_MESSAGE, null, 
									null, "");
							
							dimensione = false;
						}
					
					if ( sceltaParte != null && 
							sceltaParte.length() > 0  &&
							( (convertiParte(sceltaParte, dimensione)) < codaFile.getElem(i).getFile().length() ) &&
							( (convertiParte(sceltaParte, dimensione)) > 0 ) ) {
						
						parteValida = true;
						
						switch(Divisione.getIdDivisione(controlloDiv)) {
				
						case 0: divisioni[i] = new DivisioneParti(codaFile.getElem(i),
													convertiParte(sceltaParte, dimensione));
							break;
					
						case 1: divisioni[i] = new DivisioneCrittografata(codaFile.getElem(i), 
													convertiParte(sceltaParte, dimensione));
							break;
					
						case 2: divisioni[i] = new DivisioneCompressa(codaFile.getElem(i), 
													convertiParte(sceltaParte, dimensione));
							break;
					
						case 3: divisioni[i] = new DivisioneParti(codaFile.getElem(i), 
													(int) convertiParte(sceltaParte, dimensione));
							break;
					
						default: JOptionPane.showMessageDialog(this, 
								"<html><body><p style='width: 500px;'>Si � verificato un errore nel " + 
								"riconoscere il tipo di divisione del file " +
								codaFile.getElem(i).getFile().getName() + ".</p></body></html>", 
								"Errore", JOptionPane.ERROR_MESSAGE);
						}
					}
					else if (sceltaParte == null) {
						
						procediDiv = false;
					}
				}
				
			}
				
			if (procediDiv) {	
				
				jpb.setString(" 0 / " + contNumFile);
				
				for (int i = 0; i < contNumFile; i++) {
					
					EsecuzioneDiv div = new EsecuzioneDiv(divisioni[i]);
					div.execute();
				}
			
			}
		}	
		
		if (pulsantePremuto == unisci) {
			
			if (selezSingola.selezioneSingola()) {
				
				files = selezSingola.getFileSelezionati();
				
				try {
					
					String controlloSepar = files[0].getName().substring(Divisione.getPosizSeparatore(),
																		Divisione.getPosizSeparatore() + 1);
					int controlloNum;
				
					try {
					
						controlloNum = Integer.parseInt(files[0].getName().
								substring(Divisione.getPosizNumFile(), Divisione.getPosizNomeFile(files[0]) - 1));
					}
					catch (NumberFormatException nfe) {
					
						controlloNum = -1;
					}
				
					if ( controlloSepar.contentEquals(Divisione.getSeparatore()) && controlloNum == 1) {
				
						char controlloDiv = files[0].getName().charAt(Divisione.getPosizTipoDiv());
					
						Unione unione;
				
						switch(Unione.getIdDivisione(controlloDiv)) {
			
						case 0: unione = new UnioneParti(files[0]);
								unione.unisci();
							break;
				
						case 1: unione = new UnioneDecrittografata(files[0]);
								unione.unisci();
							break;
				
						case 2: unione = new UnioneDecompressa(files[0]);
								unione.unisci();
							break;
				
						default: JOptionPane.showMessageDialog(this, 
								"<html><body><p style='width: 500px;'>Non � stato possibile riconoscere " +
								"il tipo di divisione del file " + files[0].getName() + ".</p></body></html>", 
								"Errore", JOptionPane.ERROR_MESSAGE);
				
						}
			
					}
					else {
					
						JOptionPane.showMessageDialog(this, 
								"<html><body><p style='width: 500px;'>Il file selezionato deve costituire " + 
								"la prima parte del file originale che si vuole ricomporre, ed avere un nome " +
								"con formato \"intestazione-numero-nomeFile\".</p></body></html>", 
								"Errore", JOptionPane.ERROR_MESSAGE);
					}
				}
					
				catch (StringIndexOutOfBoundsException ex) {
					
						JOptionPane.showMessageDialog(this, 
							"<html><body><p style='width: 500px;'>Il file selezionato non � stato generato " +
							"dalla procedura di divisione.</p></body></html>", 
							"Errore", JOptionPane.ERROR_MESSAGE);
				}
					
			}
					
		}
		
	}
	

	/**
	 * Metodo che imposta i valori del {@code GridBagConstraints} specificato a quelli passati come parametro.
	 * 
	 * @param lim il {@code GridBagConstraints} specificato
	 * @param gridx il valore del campo {@code gridx} del {@code GridBagConstraints} specificato
	 * @param gridy il valore del campo {@code gridy} del {@code GridBagConstraints} specificato
	 * @param gridwidth il valore del campo {@code gridwidth} del {@code GridBagConstraints} specificato
	 * @param gridheight il valore del campo {@code gridheight} del {@code GridBagConstraints} specificato
	 * @param weightx il valore del campo {@code weightx} del {@code GridBagConstraints} specificato
	 * @param weighty il valore del campo {@code weighty} del {@code GridBagConstraints} specificato
	 * @param fill il valore del campo {@code fill} del {@code GridBagConstraints} specificato
	 */
	private void setLim(GridBagConstraints lim, int gridx, int gridy, int gridwidth, int gridheight, 
			int weightx, int weighty, int fill) {
		
		lim.gridx = gridx;
		lim.gridy = gridy;
		lim.gridwidth = gridwidth;
		lim.gridheight = gridheight;
		lim.weightx = weightx;
		lim.weighty = weighty;
		lim.fill = fill;
		
	}
	
	/**
	 * Metodo che prende in ingresso una stringa inserita da utente riguardante la dimensione della parte oppure
	 * il numero di parti (quale dei due sia � specificato dal parametro booleano {@code dimensione}) in base
	 * al quale attuare la divisione del file, e che ne estrae il valore numerico.
	 * <p>
	 * Se � una dimensione la restituisce convertita in byte, altrimenti restituisce il numero di parti inalterato.
	 * 
	 * @param parte stringa inserita da utente riguardante la dimensione della parte oppure il numero di parti
	 * 				  in base al quale attuare la divisione del file
	 * @param dimensione {@code true} se {@code parte} descrive una dimensione, {@code false} altrimenti
	 * @return la dimensione convertita in byte, oppure il numero di parti inalterato; se si � verificato
	 * 			un errore restituisce {@code -1}
	 */
	private double convertiParte(String parte, boolean dimensione) {
		
		String tmp = parte.replaceAll("[^0-9.]", "");
		boolean isNumero = true;
		
		try {
			
			Double.parseDouble(tmp);
		}
		catch (NumberFormatException nfe) {
			
			isNumero = false;
		}
		
		if (isNumero) {
			
			double num = Double.parseDouble(tmp);
			
			if (dimensione) {
				
				if ( parte.contains("kb") || parte.contains("KB") ) {
					
					return num * CONVERT_BYTE;
				}
				if ( parte.contains("mb") || parte.contains("MB") ) {
					
					return num * CONVERT_BYTE * CONVERT_BYTE;
				}
				if ( parte.contains("gb") || parte.contains("GB") ) {
					
					return num * CONVERT_BYTE * CONVERT_BYTE * CONVERT_BYTE;
				}
			}
			else {
			
				return num;
			}
		}
		
		return -1;
	}

}
