package FileSplitter;

import java.util.*;

/**
 * Classe che descrive una coda di file da dividere.
 * <p>
 * La suddetta coda � realizzata con una {@code ArrayList} in quanto varie operazioni (come la modifica, 
 * l'eliminazione e la scelta del metodo di divisione) riguardano spesso elementi che si trovano in mezzo
 * alla coda e non al suo inizio o alla sua fine.
 * 
 * @param <E> descrive il tipo di dato su cui verr� parametrizzata un'istanza della coda ({@link FileScelto}).
 * 
 * @author Laura Morselli
 *
 */
public class CodaFile<E> {
	
	/**
	 * La coda contenente i file.
	 */
	private ArrayList<E> codaFile;
	
	/**
	 * Crea una nuova istanza di {@link CodaFile}.
	 * 
	 */
	public CodaFile () {
		
		codaFile = new ArrayList<E>();
	}
	
	/**
	 * Metodo che aggiunge l'elemento specificato in fondo alla coda.
	 * 
	 * @param f l'elemento da aggiungere
	 */
	public void aggiungi(E f) {
		
		codaFile.add(f);
	}
	
	/**
	 * Metodo che aggiunge l'elemento specificato alla posizione specificata nella coda.
	 * 
	 * @param index posizione in cui l'elemento verr� inserito
	 * @param f l'elemento da inserire
	 */
	public void aggiungi(int index, E f) {
		
		codaFile.add(index, f);
	}
	
	/**
	 * Metodo che rimuove l'elemento specificato dalla coda.
	 * 
	 * @param f l'elemento da rimuovere
	 */
	public void rimuovi(E f) {
		
		codaFile.remove(f);
	}
	
	/**
	 * Metodo che rimuove il primo elemento della coda.
	 */
	public void rimuoviTesta() {
		
		codaFile.remove(0);
	}
	
	/**
	 * Metodo che restituisce l'elemento presente alla posizione specificata nella coda.
	 * 
	 * @param index la posizione dell'elemento da restituire
	 * @return l'elemento alla posizione specificata
	 */
	public E getElem(int index) {
		
		return (E)codaFile.get(index);
	}
	
	/**
	 * Metodo che restituisce la dimensione della coda.
	 * 
	 * @return la dimensione della coda
	 */
	public int getDim() {
		
		return codaFile.size();
	}
	
}
