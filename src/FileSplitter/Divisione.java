package FileSplitter;

import java.io.*;

/**
 * Classe base per una gerarchia di tipi di divisione dei file, che definisce un generico tipo di divisione.
 * 
 * @author Laura Morselli
 *
 */
public abstract class Divisione {
	
	/**
	 * Definisce i tipi di divisione disponibili.
	 * <p>
	 * Ognuno di essi possiede una descrizione ed un carattere che rappresenta l'intestazione da apporre
	 * ai nomi dei file creati attraverso il processo di divisione.
	 * 
	 * @author Laura Morselli
	 *
	 */
	public enum tipoDivisione {
		/**
		 * Tipo di divisione in cui l'utente specifica la dimensione che i file generati dalla divisione
		 * dovranno avere.
		 * <p>
		 * 'd' � l'intestazione che verr� apposta ai nomi dei file creati in questo modo.
		 */
		DIM_PARTI ("Divisione specificando la dimensione delle parti", 'd'), 
		/**
		 * Tipo di divisione in cui il contenuto del file originale � crittografato, ed in cui 
		 * l'utente specifica la dimensione che i file generati dalla divisione dovranno avere.
		 * <p>
		 * 'c' � l'intestazione che verr� apposta ai nomi dei file creati in questo modo.
		 */
		CRITTOGRAFIA ("Divisione crittografata specificando la dimensione delle parti", 'c'),
		/**
		 * Tipo di divisione in cui il contenuto del file originale viene compresso, ed in cui 
		 * l'utente specifica la dimensione che i file generati dalla divisione dovranno avere.
		 * <p>
		 * 'z' � l'intestazione che verr� apposta ai nomi dei file creati in questo modo.
		 */
		COMPRESSIONE ("Divisione con compressione specificando la dimensione delle parti", 'z'),
		/**
		 * Tipo di divisione in cui l'utente specifica il numero di parti in cui dovr� essere diviso il file.
		 * <p>
		 * 'p' � l'intestazione che verr� apposta ai nomi dei file creati in questo modo.
		 */
		NUM_PARTI ("Divisione specificando il numero di parti", 'p');
		
		/**
		 * La descrizione del tipo di divisione.
		 */
		private String desc;
		/**
		 * L'intestazione associata al tipo di divisione.
		 */
		private char intest;
		
		/**
		 * Crea una nuova istanza di {@link tipoDivisione} con la descrizione e l'intestazione specificati.
		 * 
		 * @param desc la descrizione del tipo di divisione
		 * @param intest l'intestazione del tipo di divisione
		 */
		private tipoDivisione(String desc, char intest) {
			
			this.desc = desc;
			this.intest = intest;
		}
		
		/**
		 * Metodo che restituisce la variabile {@link #desc}.
		 * 
		 * @return la variabile {@link #desc}
		 */
		public String getDescDiv() {
			
			return desc;
		}
		
		/**
		 * Metodo che restituisce la variabile {@link #intest}.
		 * 
		 * @return la variabile {@link #intest}
		 */
		public char getIntest() {
			
			return intest;
		}
	}
	
	/**
	 * Costante che, nei nomi dei file risultanti dalla divisione, rappresenta il tipo di 
	 * carattere utilizzato come separatore fra parole.
	 */
	private static final String SEPARATORE = "-";
	/**
	 * Costante che, nei nomi dei file risultanti dalla divisione, rappresenta la posizione
	 * del carattere indicante il {@link tipoDivisione tipo di divisione} utilizzato.
	 */
	private static final int POSIZ_TIPO_DIV = 0;
	/**
	 * Costante che, nei nomi dei file risultanti dalla divisione, rappresenta la posizione della prima istanza
	 * del carattere utilizzato come separatore.
	 */
	private static final int POSIZ_SEPARATORE = 1;
	/**
	 * Costante che, nei nomi dei file risultanti dalla divisione, rappresenta la posizione da cui inizia il
	 * numero indicante di quale parte del file originale si tratta.
	 */
	private static final int POSIZ_NUM_FILE = 2;
	
	/**
	 * Il {@link FileScelto} sui cui attuare la divisione.
	 */
	private FileScelto fileScelto;
	/**
	 * La dimensione del file su cui attuare la divisione.
	 */
	private double dimFile; 
	/**
	 * La dimensione che ogni file originato dalla divisione dovr� avere.
	 */
	private double dimParte;
	/**
	 * Il numero di parti in cui verr� diviso il file scelto per la divisione.
	 */
	private int numParti;
	
	/**
	 * Crea una nuova istanza di {@link Divisione} con il {@link FileScelto} specificato.
	 * 
	 * @param fileScelto un'istanza di {@link FileScelto}
	 */
	public Divisione(FileScelto fileScelto) {
		
		this.fileScelto = fileScelto;
		this.dimFile = fileScelto.getFile().length();
	}

	/**
	 * Metodo che attua la divisione di un file in pi� parti.
	 */
	public abstract void dividi();
	
	/**
	 * Metodo che restituisce il file associato alla variabile {@link #fileScelto}.
	 * 
	 * @return il file associato alla variabile {@link #fileScelto}
	 */
	public File getFile() {
		
		return this.fileScelto.getFile();
	}
	
	/**
	 * Metodo che restituisce la variabile {@link #dimFile}.
	 * 
	 * @return la variabile {@link #dimFile}
	 */
	public double getDimFile() {
		
		return this.dimFile;
	}
	
	/**
	 * Metodo che restituisce la variabile {@link #dimParte}.
	 * 
	 * @return la variabile {@link #dimParte}
	 */
	public double getDimParte() {
		
		return this.dimParte;
	}
	
	/**
	 * Metodo che imposta la dimensione della variabile {@link #dimParte} a quella specificata.
	 * 
	 * @param dim il nuovo valore della variabile {@link #dimParte}
	 */
	public void setDimParte(double dim) {
		
		this.dimParte = dim;
	}
	
	/**
	 * Metodo che restituisce la variabile {@link #numParti}.
	 * 
	 * @return la variabile {@link #numParti}
	 */
	public int getNumParti() {
		
		return this.numParti;
	}
	
	/**
	 * Metodo che imposta il valore della variabile {@link #numParti} a quello specificato.
	 * 
	 * @param num il nuovo valore della variabile {@link #numParti}
	 */
	public void setNumParti(int num) {
		
		this.numParti = num;
	}
	
	/**
	 * Metodo che ritorna il numero d'ordinamento del {@link tipoDivisione tipo di divisione} 
	 * corrispondente alla stringa ricevuta in input, se quest'ultima corrisponde ad una delle 
	 * descrizioni del tipo di divisione; altrimenti restituisce {@code -1}.
	 * 
	 * @param s una stringa che rappresenta la descrizione di uno dei tipi di divisione
	 * @return il numero d'ordinamento del tipo di divisione corrispondente alla descrizione, altrimenti {@code -1}
	 */
	public static int getIdDivisione(String s) {
		
		if ( s.equals(tipoDivisione.DIM_PARTI.getDescDiv()) )
			return tipoDivisione.DIM_PARTI.ordinal();
		
		if ( s.equals(tipoDivisione.CRITTOGRAFIA.getDescDiv()) )
			return tipoDivisione.CRITTOGRAFIA.ordinal();
		
		if ( s.equals(tipoDivisione.COMPRESSIONE.getDescDiv()) )
			return tipoDivisione.COMPRESSIONE.ordinal();
		
		if ( s.equals(tipoDivisione.NUM_PARTI.getDescDiv()) )
			return tipoDivisione.NUM_PARTI.ordinal();
		else
			return -1;
		
	}
	
	
	/**
	 * Metodo che restituisce la costante {@link #SEPARATORE}.
	 * 
	 * @return la costante {@link #SEPARATORE}
	 */
	public static String getSeparatore() {
		
		return SEPARATORE;
	}
	
	/**
	 * Metodo che restituisce la costante {@link #POSIZ_TIPO_DIV}.
	 * 
	 * @return la costante {@link #POSIZ_TIPO_DIV}
	 */
	public static int getPosizTipoDiv() {
		
		return POSIZ_TIPO_DIV;
	}
	
	/**
	 * Metodo che restituisce la costante {@link #POSIZ_SEPARATORE}.
	 * 
	 * @return la costante {@link #POSIZ_SEPARATORE}
	 */
	public static int getPosizSeparatore() {
		
		return POSIZ_SEPARATORE;
	}
	
	/**
	 * Metodo che restituisce la costante {@link #POSIZ_NUM_FILE}.
	 * 
	 * @return la costante {@link #POSIZ_NUM_FILE}
	 */
	public static int getPosizNumFile() {
		
		return POSIZ_NUM_FILE;
	}
	
	/**
	 * Metodo che restituisce la posizione, nel nome di un file generato dalla divisione, del punto da cui 
	 * inizia il nome del file originale (cio� dopo la prima istanza di {@link #SEPARATORE} a partire da 
	 * {@link #POSIZ_NUM_FILE}).
	 * 
	 * @param file il file generato da una procedura di divisione e nel quale si vuole cercare la posizione
	 * 				da cui inizia il nome del file originale
	 * @return la posizione, nel nome del file generato dalla divisione, da cui inizia il nome
	 * 			del file originale
	 */
	public static int getPosizNomeFile(File file) {
		
		return file.getName().indexOf(SEPARATORE, POSIZ_NUM_FILE) + 1;
	}


}
