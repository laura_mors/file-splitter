package FileSplitter;

import javax.swing.*;

/**
 * Classe che implementa il metodo {@code main} dell'applicazione.
 * 
 * @author Laura Morselli
 *
 */
public class Main {

	/**
	 * Il metodo {@code main} dell'applicazione, da cui viene creata la finestra contenente 
	 * l'{@link PannelloPrincipale interfaccia grafica} dell'applicazione.
	 * 
	 */
	public static void main(String[] args) {
		
		JFrame f = new JFrame("File Splitter");
		f.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		
		JPanel p = new PannelloPrincipale();
		f.add(p);
		f.pack();
		f.setExtendedState(JFrame.MAXIMIZED_BOTH);
		f.setVisible(true);

	}

}
