package FileSplitter;

import java.io.*;

import javax.swing.JOptionPane;

import static FileSplitter.Divisione.tipoDivisione.*;

/**
 * Classe base per una gerarchia di tipi di unione dei file, che definisce un generico tipo di unione.
 * 
 * @author Laura Morselli
 *
 */
public abstract class Unione {
	
	/**
	 * Il file che costituisce la prima parte del file originale da ricomporre, e da cui partir� l'unione.
	 */
	private File file;
	/**
	 * Carattere che identifica il tipo di divisione con cui � stato diviso il file originale.
	 */
	private char tipoDiv;
	/**
	 * Numero di parti in cui � stato diviso il file.
	 */
	private int numPartiFile;
	/**
	 * Array contenente le varie parti in cui � stato diviso il file originale.
	 */
	private File[] partiFile;
	
	/**
	 * Crea una nuova istanza di {@link Unione} con il file specificato, e dal quale avr�
	 * inizio la ricomposizione del file originale. 
	 * <p>
	 * Il tipo di divisione con il quale il suddetto file � stato diviso � ritrovabile nel nome
	 * alla posizione data da {@link Divisione#getPosizTipoDiv() getPosizTipoDiv()}.
	 * <p>
	 * Le parti che compongono il file originale sono trovate grazie a 
	 * {@link #trovaPartiFile(File, int)}.
	 * 
	 * @param file il file da quale avr� inizio la ricomposizione del file originale
	 */
	public Unione(File file) {
		
		this.file = file;
		this.tipoDiv = file.getName().charAt(Divisione.getPosizTipoDiv());
		this.numPartiFile = contaPartiFile(this.file);
		this.partiFile = trovaPartiFile(this.file, this.numPartiFile);

	}
	
	/**
	 * Metodo che unisce le varie parti di un file precedentemente diviso, ottenendo cos� il file originale.
	 */
	public abstract void unisci();
	
	/**
	 * Metodo che controlla se un file � effettivamente una parte del file originale che si vuole ricomporre.
	 * <p>
	 * Il primo ed il secondo file passati come argomento sono, rispettivamente, la prima parte del file che 
	 * si vuole ricomporre, ed un file che si supponga faccia parte dell'insieme di parti costituenti il file
	 * originale.
	 * <p>
	 * Se l'intestazione del {@link Divisione.tipoDivisione tipo di divisione } ed i loro nomi corrispondono, e
	 * se il nome del secondo file contiene un numero nella posizione prevista, allora viene restituito {@code true};
	 * {@code false} altrimenti.
	 * 
	 * @param filePrimaParte file corrispondente alla prima parte del file che si vuole ricomporre
	 * @param file un file da controllare e che si supponga faccia parte dell'insieme di parti costituenti il file
	 * 				originale
	 * @return {@code true} se il secondo file fa effettivamente parte dell'insieme di parti del file originale,
	 * 			{@code false} altrimenti.
	 */
	public boolean isParteFileOriginale(File filePrimaParte, File file) {
		
		String nomeFilePrimo = filePrimaParte.getName();		
		String nomeFile = file.getName();
		
		if (nomeFile.charAt(Divisione.getPosizTipoDiv()) == 
				nomeFilePrimo.charAt(Divisione.getPosizTipoDiv())) {
			
			if (nomeFile.contains(
					nomeFilePrimo.substring(Divisione.getPosizNomeFile(filePrimaParte), nomeFilePrimo.length()))) {
				
				try {
					
					Integer.parseInt(nomeFile.substring(
							Divisione.getPosizNumFile(), Divisione.getPosizNomeFile(file) - 1));
				}
				catch (NumberFormatException nfe) {
					
					return false;
				}
				
				return true;
			}
		}
		
		return false;
	}
	
	/**
	 * Metodo che conta da quante parti � costituito il file originale, partendo dal file passato come argomento
	 * (che costituisce la prima parte). 
	 * <p>
	 * Ogni file nella directory genitore di {@code filePrimaParte} che soddisfa le condizioni stabilite dal metodo
	 * {@link #isParteFileOriginale(File, File)} contribuisce al numero totale che verr� restituito.
	 * 
	 * @param filePrimaParte il file costituente la prima parte del file originale che si vuole ricostruire
	 * @return il numero di parti costituenti il file originale
	 */
	public int contaPartiFile(File filePrimaParte) {
		
		int numFile = 0;
		
		for (File f : filePrimaParte.getParentFile().listFiles()) {
			
			if (f.isFile()) {
				
				if( isParteFileOriginale(filePrimaParte, f) ) {
					numFile++;
				}
			}
		}
		
		return numFile;
	}
	
	/**
	 * Metodo che, partendo dal file specificato (il quale deve essere la prima parte del file
	 * originale), trova le varie parti in cui un file � stato diviso, e le restituisce ordinate in un array di
	 * dimensione equivalente al numero di parti passato come argomento.
	 * 
	 * @param filePrimaParte il file che costituisce la prima parte di un file precedentemente diviso
	 * @param numPartiFile il numero di parti in cui il file originale � stato diviso
	 * @return le parti del file originale ordinate in un array
	 */
	public File[] trovaPartiFile(File filePrimaParte, int numPartiFile) {
		
		partiFile = new File[numPartiFile];
		
		for (File f : filePrimaParte.getParentFile().listFiles()) {
			
			if (f.isFile()) {
				
				if( isParteFileOriginale(filePrimaParte, f) ) {
					
					int posizioneFileTrovato = Integer.parseInt(f.getName().substring(
							Divisione.getPosizNumFile(), Divisione.getPosizNomeFile(f) - 1));
					
					partiFile[posizioneFileTrovato-1] = f;
				}
			}
		}
		return partiFile;
	}
	
	/**
	 * Metodo che comunica su schermo, utilizzando un {@code JOptionPane}, il completamento dell'unione
	 * e la creazione del file il cui nome � passato come argomento.
	 *  
	 * @param nomeFileUnito il nome del file creato dalla procedura di unione
	 */
	public static void mostraMessaggioCompletamento(String nomeFileUnito) {
		
		JOptionPane.showMessageDialog(null, 
			"<html><body><p style='width: 500px;'>Unione completata. Il file creato � " +
			nomeFileUnito + ".", 
			"Esito unione", JOptionPane.INFORMATION_MESSAGE);
	}
	
	/**
	 * Metodo che restituisce la variabile {@link #file}.
	 * 
	 * @return la variabile {@link #file}
	 */
	public File getFile() {
		
		return this.file;
	}
	
	/**
	 * Metodo che restituisce il nome del file originale, estraendolo dal nome di {@link #file}.
	 * 
	 * @return il nome del file originale
	 */
	public String getNomeOriginale() {
		
		return this.file.getName().substring(Divisione.getPosizNomeFile(this.file));
	}
	
	/**
	 * Metodo che restituisce la variabile {@link #tipoDiv}.
	 * 
	 * @return la variabile {@link #tipoDiv}
	 */
	public char getTipoDiv() {
		
		return this.tipoDiv;
	}
	
	/**
	 * Metodo che restituisce la variabile {@link #numPartiFile}.
	 * 
	 * @return la variabile {@link #numPartiFile}
	 */
	public int getNumPartiFile() {
		
		return this.numPartiFile;
	}
	
	/**
	 * Metodo che restituisce la variabile {@link #partiFile}.
	 * 
	 * @return la variabile {@link #partiFile}
	 */
	public File[] getPartiFile() {
		
		return this.partiFile;
	}
	
	/**
	 * Metodo che prende in ingresso un carattere e restituisce il numero d'ordinamento del 
	 * {@link Divisione.tipoDivisione tipo di divisione} corrispondente se il suddetto carattere corrisponde 
	 * ad una delle intestazioni dei tipi di divisione, altrimenti restituisce {@code -1}.
	 * 
	 * @param c un carattere che rappresenta l'intestazione di uno dei tipi di divisione
	 * @return il numero d'ordinamento del tipo di divisione corrispondente, altrimenti {@code -1}
	 */
	public static int getIdDivisione(char c) {
		/* 
		 * Viene restituito il numero di ordinamento di DIM_PARTI per entrambi in quanto la
		 * procedura di unione � esattamente la stessa
		 */
		if (c == DIM_PARTI.getIntest() || c == NUM_PARTI.getIntest()) 
			return DIM_PARTI.ordinal();
		
		if ( c == CRITTOGRAFIA.getIntest()) 
			return CRITTOGRAFIA.ordinal();
		
		if (c == COMPRESSIONE.getIntest()) 
			return COMPRESSIONE.ordinal();
		
		else
			return -1;
	}
	

}
