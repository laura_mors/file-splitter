package FileSplitter;

/**
 * Interfaccia utilizzabile da una classe che effettua la {@link Divisione} di un file in base
 * alla dimensione che ognuno dei file risultanti dovr� avere.
 * 
 * @author Laura Morselli
 *
 */
public interface DivisionePerDim {
	
	/**
	 * Metodo per calcolare il numero di parti in cui verr� diviso un file, conoscendo la dimensione che 
	 * la singola parte dovr� avere.
	 * 
	 * @param dimParte la dimensione di ognuno dei file generati dalla divisione
	 * @return il numero di parti in cui verr� diviso un file
	 */
	public int calcolaNumParti(double dimParte);

}
