package FileSplitter;

import java.io.*;
import java.security.*;
import javax.crypto.*;
import javax.crypto.spec.*;
import javax.swing.JOptionPane;

/**
 * Classe che descrive una modalit� di unione dei file in cui le varie parti di un file, 
 * precedentemente crittografato e diviso, vengono decrittografate ed unite per ricostruire il file originale.
 * <p>
 * Eredita da {@link UnioneParti}.
 * 
 * @author Laura Morselli
 *
 */
public class UnioneDecrittografata extends UnioneParti {
	
	/**
	 * La chiave utilizzata per la decifratura.
	 */
	private Key key;
	/**
	 * L'algoritmo utilizzato per la decifratura.
	 */
	private Cipher cipher;
	/**
	 * L'array utilizzato per leggere il contenuto cifrato di un file.
	 */
	private byte[] inputBytes;

	/**
	 * Crea una nuova istanza di {@link UnioneDecrittografata} con il file specificato, costituente
	 * la prima parte del file da ricostruire.
	 * 
	 * @param file il file da cui verr� avviata l'unione
	 */
	public UnioneDecrittografata(File file) {
		
		super(file);
	}
	
	/**
	 * Metodo che decifra ed unisce le varie parti di un file precedentemente crittografato
	 * e diviso, ottenendo cos� il file originale. La ricomposizione verr� avviata dal file fornito quando 
	 * si � creata un'istanza di {@link UnioneDecrittografata}.
	 * <p>
	 * Utilizza un {@code FileInputStream} per leggere i vari file, ed un {@code FileOutputStream} per scrivere
	 * ed unire il loro contenuto decrittografato in un unico file.
	 */
	@Override
	public void unisci() {
		
		try {
			
			key = new SecretKeySpec(DivisioneCrittografata.getKey().getBytes(), DivisioneCrittografata.getCipher());
			cipher = Cipher.getInstance(DivisioneCrittografata.getCipher());
			cipher.init(Cipher.DECRYPT_MODE, key);	
		}
		catch (GeneralSecurityException se) {
			
			se.printStackTrace();
		}
		
		try {
			
			File fileCompleto = CreazioneFile.creaNuovoFileUnione(super.getFile());
			
			FileOutputStream fos = new FileOutputStream(fileCompleto, true);
			
			int bytesLetti = 0;
			
			for (int i = 0; i < super.getNumPartiFile(); i++) {
				
				FileInputStream fis = new FileInputStream(super.getPartiFile()[i]);	
				
				inputBytes = new byte[(int) super.getPartiFile()[i].length()];
				
				while ( ( ( bytesLetti = fis.read(inputBytes) ) != -1 ) && 
						bytesLetti != 0 ) {
		
					try {
					
					fos.write(cipher.doFinal(inputBytes, 0, bytesLetti));
					}
					
					catch (GeneralSecurityException se) {
						
						se.printStackTrace();
					}

				}
				
				fis.close();	
			}
			
			fos.close();
			
			Unione.mostraMessaggioCompletamento(fileCompleto.getName());
		}
		
		catch(IOException e) {
			
			JOptionPane.showMessageDialog(null, 
				"<html><body><p style='width: 500px;'>Si � verificato un errore durante " + 
				"l'unione decrittografata del file " + super.getFile().getName() + ".", 
				"Errore", JOptionPane.ERROR_MESSAGE);
		}
	}
	

}
