package FileSplitter;

import java.io.*;

import java.security.*;
import javax.crypto.*;
import javax.crypto.spec.*;
import javax.swing.JOptionPane;

/**
 * Classe che descrive una modalit� di divisione dei file basata sul dividerli in pi� parti, 
 * ognuna memorizzata in un file diverso, e in cui il contenuto di ogni file � crittografato.
 * <p>
 * Eredita da {@link DivisioneParti}.
 * 
 * @author Laura Morselli
 *
 */
public class DivisioneCrittografata extends DivisioneParti {
	
	/**
	 * Costante che rappresenta il valore della chiave utilizzata per la crittografia. 
	 */
	private final static String KEY = "6322810319465293";
	
	/**
	 * Costante che rappresenta il nome dell'algoritmo utilizzato per la crittografia.
	 */
	private final static String CIPHER = "AES";
	
	/**
	 * La chiave utilizzata per la crittografia.
	 */
	private Key key;
	/**
	 * L'algoritmo utilizzato per la crittografia.
	 */
	private Cipher cipher;
	/**
	 * L'array utilizzato per leggere il contenuto di un file.
	 */
	private byte[] inputBytes = new byte[super.getInputArrDim()];

	/**
	 * Crea una nuova istanza di {@link DivisioneCrittografata} con il {@link FileScelto} e
	 * la dimensione della singola parte specificati.
	 * 
	 * @param file il {@link FileScelto} su cui verr� attuata la divisione
	 * @param dimParte la dimensione che ognuno dei file generati dovr� avere
	 */
	public DivisioneCrittografata(FileScelto file, double dimParte) {
		
		super(file, dimParte);
	}

	/**
	 * Metodo che attua la divisione di un file in pi� parti crittografandone il contenuto. Il numero di file 
	 * generati dipende dalla dimensione fornita quando si � creata un'istanza di {@link DivisioneCrittografata}.
	 * <p>
	 * Utilizza un {@code FileInputStream} per leggere dal file originario, ed un {@code FileOutputStream}
	 * per scrivere il contenuto crittografato sui file generati.
	 */
	@Override
	public void dividi() {
		
		try {
			
			key = new SecretKeySpec(KEY.getBytes(), CIPHER);
			cipher = Cipher.getInstance(CIPHER);
			cipher.init(Cipher.ENCRYPT_MODE, key);
		}
		catch (GeneralSecurityException se) {
			
			se.printStackTrace();
		}
		
		try {
			
			File nuovaCartella = CreazioneFile.creaNuovaCartella(super.getFile(), 
					tipoDivisione.CRITTOGRAFIA.getIntest());
			
			FileInputStream fis = new FileInputStream(super.getFile());
			
			int bytesLetti = 0;	
		
			for (int i = 0; i < super.getNumParti(); i++) {
				
				File nuovoFile = CreazioneFile.creaNuovoFileDiv(nuovaCartella, super.getFile().getName(),
						tipoDivisione.CRITTOGRAFIA.getIntest(), i+1);
			
				FileOutputStream fos = new FileOutputStream(nuovoFile, true);
			
				if ( bytesLetti != -1 && bytesLetti != 0 ) {
					
					fos.write(cipher.update(inputBytes, 0, bytesLetti));
				}
			
			
				while ( ( ( ( bytesLetti = fis.read(inputBytes) ) != -1 ) && 
						( bytesLetti != 0 ) &&
						( ( nuovoFile.length()) <= super.getDimParte() ) ) ) {
				
					fos.write(cipher.update(inputBytes, 0, bytesLetti));
				
					bytesLetti = 0;	
				}
			
				try {
				
					fos.write(cipher.doFinal());
				}
				catch (GeneralSecurityException se) {
					
					se.printStackTrace();
				}
			
				fos.close();
			}
		
			fis.close();
		}
		catch (IOException e) {
			
			JOptionPane.showMessageDialog(null, 
				"<html><body><p style='width: 500px;'>Si � verificato un errore durante " + 
				"la divisione crittografata del file " + super.getFile().getName() + ".", 
				"Errore", JOptionPane.ERROR_MESSAGE);
		}
		
	}
	
	/**
	 * Metodo che restituisce la costante {@link #KEY}.
	 * 
	 * @return la costante {@link #KEY}
	 */
	protected static String getKey() {
		
		return KEY;
	}
	
	/**
	 * Metodo che restituisce la costante {@link #CIPHER}.
	 * 
	 * @return la costante {@link #CIPHER}.
	 */
	protected static String getCipher() {
		
		return CIPHER;
	}
	
	
}
