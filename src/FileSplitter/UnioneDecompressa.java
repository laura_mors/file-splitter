package FileSplitter;

import java.io.*;
import java.util.zip.*;

import javax.swing.JOptionPane;

/**
 * Classe che descrive una modalit� di unione dei file in cui le varie parti di un file, 
 * precedentemente compresso e diviso, vengono decompresse ed unite per ricostruire il file originale.
 * <p>
 * Eredita da {@link UnioneParti}.
 * 
 * @author Laura Morselli
 *
 */
public class UnioneDecompressa extends UnioneParti {

	/**
	 * L'array utilizzato per leggere il contenuto di un file.
	 */
	private byte[] inputBytes;
	
	/**
	 * Crea una nuova istanza di {@link UnioneDecompressa} con il file specificato, costituente
	 * la prima parte del file da ricostruire.
	 * 
	 * @param file il file da cui verr� avviata l'unione
	 */
	public UnioneDecompressa(File file) {
		
		super(file);	
	}
	
	/**
	 * Metodo che decomprime ed unisce le varie parti di un file precedentemente compresso
	 * e diviso, ottenendo cos� il file originale. La ricomposizione verr� avviata dal file fornito quando 
	 * si � creata un'istanza di {@link UnioneDecompressa}.
	 * <p>
	 * Utilizza un {@code FileInputStream} contenuto in uno {@code ZipInputStream} per leggere e decomprimere 
	 * i vari file, ed un {@code FileOutputStream} per scrivere ed unire il loro contenuto in un unico file.
	 * 
	 */
	@Override
	public void unisci() {
		
		try {
			
			File fileCompleto = CreazioneFile.creaNuovoFileUnione(super.getFile());
			
			FileOutputStream fos = new FileOutputStream(fileCompleto, true);
			
			int bytesLetti = 0;
			
			for (int i = 0; i < super.getNumPartiFile(); i++) {
				
				FileInputStream fis = new FileInputStream(super.getPartiFile()[i]);
				
				ZipInputStream zis = new ZipInputStream(fis);	
				
				zis.getNextEntry();
				
				inputBytes = new byte[(int) super.getPartiFile()[i].length()];
				
				while  ( ( bytesLetti = zis.read(inputBytes) ) != -1 && 
						bytesLetti != 0) {
					
					fos.write(inputBytes, 0, bytesLetti);	
				}
				
				zis.closeEntry();
				zis.close();
				fis.close();
			}
			
			fos.close();
			
			Unione.mostraMessaggioCompletamento(fileCompleto.getName());
		}
		catch(IOException e) {  
			
			JOptionPane.showMessageDialog(null, 
				"<html><body><p style='width: 500px;'>Si � verificato un errore durante " + 
				"l'unione con decompressione del file " + super.getFile().getName() + ".", 
				"Errore", JOptionPane.ERROR_MESSAGE);
		}
	}

}
