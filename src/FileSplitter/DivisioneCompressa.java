package FileSplitter;

import java.io.*;
import java.util.zip.*;

import javax.swing.JOptionPane;


/**
 * Classe che descrive una modalit� di divisione dei file basata sul dividerli in pi� parti, 
 * ognuna memorizzata in un file diverso, e in cui il contenuto di ogni file � compresso.
 * <p>
 * Eredita da {@link DivisioneParti}.
 * 
 * @author Laura Morselli
 *
 */
public class DivisioneCompressa extends DivisioneParti {
	
	/**
	 * L'array utilizzato per leggere i byte di un file.
	 */
	private byte[] inputBytes = new byte[super.getInputArrDim()];
	
	/**
	 * Crea una nuova istanza di {@link DivisioneCompressa} con il {@link FileScelto} e
	 * la dimensione della singola parte specificati.
	 * 
	 * @param file il {@link FileScelto} su cui verr� attuata la divisione
	 * @param dimParte la dimensione che ognuno dei file generati dovr� avere
	 */
	public DivisioneCompressa(FileScelto file, double dimParte) {
		
		super(file, dimParte);
	}

	/**
	 * Metodo che attua la divisione di un file in pi� parti, comprimendone il contenuto. Il numero di file generati
	 * dipende dalla dimensione fornita quando si � creata un'istanza di {@link DivisioneCompressa}.
	 * <p>
	 * Utilizza un {@code FileInputStream} per leggere dal file originario, ed un {@code FileOutputStream}
	 * contenuto in uno {@code ZipOutputStream} per scrivere il contenuto compresso sui file generati.
	 */
	@Override
	public void dividi() {
		
		try {
			
			File nuovaCartella = CreazioneFile.creaNuovaCartella(super.getFile(), 
					tipoDivisione.COMPRESSIONE.getIntest());
			
			FileInputStream fis = new FileInputStream(super.getFile());
			
			int bytesLetti = 0;
		
			for (int i = 0; i < super.getNumParti(); i++) {
				
				ZipEntry ze = new ZipEntry(super.getFile().getName());
				
				File nuovoFile = CreazioneFile.creaNuovoFileDiv(nuovaCartella, super.getFile().getName(), 
						tipoDivisione.COMPRESSIONE.getIntest(), i+1);
			
				FileOutputStream fos = new FileOutputStream(nuovoFile, true);
				
				ZipOutputStream zos = new ZipOutputStream(fos);
				
				zos.putNextEntry(ze);
				
				if ( bytesLetti != -1 && bytesLetti != 0 ) {
					
					zos.write(inputBytes, 0, bytesLetti);
				}
			
				while ( ( ( ( bytesLetti = fis.read(inputBytes) ) != -1 ) && 
						( bytesLetti != 0 ) && 
						( ( nuovoFile.length()) <= super.getDimParte() ) ) )  {
				
					zos.write(inputBytes, 0, bytesLetti);
				
					bytesLetti = 0;
				}
			
				zos.close();	
			}
		
			fis.close();	
		}
		catch (IOException e) {
			
			JOptionPane.showMessageDialog(null, 
				"<html><body><p style='width: 500px;'>Si � verificato un errore durante " + 
				"la divisione con compressione del file " + super.getFile().getName() + ".", 
				"Errore", JOptionPane.ERROR_MESSAGE);
		}
		
	}

}
