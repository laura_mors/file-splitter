package FileSplitter;

import java.io.*;
import javax.swing.*;

import FileSplitter.Divisione.tipoDivisione;

/**
 * Classe contenente metodi per la creazione dei file (e relative cartelle) generati dalle procedure di 
 * {@link Divisione} e {@link Unione}.
 * <p>
 * Dichiarata con la parola chiave {@code final} in quanto non prevede eventuali estensioni.
 * 
 * @author Laura Morselli
 *
 */
public final class CreazioneFile {
	
	/**
	 * Costruttore di {@link CreazioneFile}, vuoto e dichiarato con la parola chiave {@code private} per evitare
	 * che la classe venga istanziata.
	 */
	private CreazioneFile() {}
	
	/**
	 * Metodo che crea una nuova cartella (vuota) nella directory genitore del file passato come argomento.
	 * Il nome del suddetto file concorre a formare il nome della cartella, assieme al 
	 * {@link tipoDivisione tipo di divisione} utilizzato.
	 * <p>
	 * Se la cartella esiste gi�, o se per qualche motivo non � stato possibile crearla, allora viene restituita
	 * la directory genitore del file utilizzato.
	 * 
	 * @param sorgente il file che fornisce il percorso ed il nome in base ai quali creare la nuova cartella
	 * @param tipoDiv carattere che rappresenta il {@link tipoDivisione tipo di divisione} usato per i file
	 * 					che saranno contenuti successivamente nella cartella
	 * @return la cartella creata, altrimenti la directory genitore del file passato come argomento
	 */
	public static File creaNuovaCartella(File sorgente, char tipoDiv) {
		
		File nuovaCartella = new File(sorgente.getParentFile(), 
				tipoDiv + Divisione.getSeparatore() + sorgente.getName() + Divisione.getSeparatore() + "diviso");
		
		if (!nuovaCartella.exists()) {
			
            if (nuovaCartella.mkdir()) {
            	
                return nuovaCartella;
            } 
        }
		
		return sorgente.getParentFile();	
		
	}
	
	/**
	 * Metodo che crea un nuovo file utilizzabile nella procedura di {@link Divisione}. 
	 * <p>
	 * Il suo nome � creato usando il nome, il tipo di divisione e il numero d'ordinamento passati come parametro 
	 * e invocando {@link #creaNomeFileDiv(char, int, String)}, mentre la cartella fornita indica il percorso in 
	 * cui dovr� essere creato il file.
	 * 
	 * @param cartella la cartella che dovr� contenere il nuovo file
	 * @param nomeSorgente il nome del file originale su cui sar� eseguita la divisione, e che sar� 
	 * 					 	riutilizzato dal nuovo file
	 * @param tipoDiv carattere che rappresenta il {@link tipoDivisione tipo di divisione} usato
	 * @param numOrdine il numero che indica l'ordine in cui il file generato dalla divisione � stato creato
	 * @return un nuovo file utilizzabile nella procedura di {@link Divisione}
	 */
	public static File creaNuovoFileDiv(File cartella, String nomeSorgente, char tipoDiv, int numOrdine) {
		
		String nomeNuovoFile = creaNomeFileDiv(tipoDiv, numOrdine, nomeSorgente);

		return creaFileFisico(cartella, nomeNuovoFile);
		
	}
	
	/**
	 * Metodo che crea un nuovo file utilizzabile nella procedura di {@link Unione}.
	 * <p>
	 * Il suo nome � creato utilizzando il nome del file passato come parametro e invocando 
	 * {@link #creaNomeFileUnione(String)}; la directory genitore del suddetto file sar� anche la 
	 * directory genitore del nuovo file.
	 * 
	 * @param sorgente il file da cui sar� avviata la procedura di {@link Unione}, e che fornisce nome e
	 * 					directory genitore per il nuovo file
	 * @return un nuovo file utilizzabile nella procedura di {@link Unione}
	 */
	public static File creaNuovoFileUnione(File sorgente) {
		
		String nomeNuovoFile = creaNomeFileUnione(
				sorgente.getName().substring(Divisione.getPosizNomeFile(sorgente), sorgente.getName().length())) ;
		
		return creaFileFisico(sorgente.getParentFile(), nomeNuovoFile);
		
	}
	
	/**
	 * Metodo che crea il nome di un file generato dalla {@link Divisione} con l'intestazione, il numero d'ordine
	 * e il nome specificati.
	 * 
	 * @param intest l'intestazione corrispondente al {@link tipoDivisione tipo di divisione} utilizzato
	 * @param numParte il numero che indica l'ordine in cui il file generato dalla divisione � stato creato
	 * @param nomeFile il nome del file originale
	 * @return il nome completo del nuovo file
	 */
	private static String creaNomeFileDiv(char intest, int numParte, String nomeFile) {
		
		return (intest + Divisione.getSeparatore() + numParte + Divisione.getSeparatore() + nomeFile);
		
	}
	
	/**
	 * Metodo che crea il nome di un file generato dall'{@link Unione} con il nome specificato.
	 * 
	 * @param nome il nome del file da cui � partita la procedura di {@link Unione}
	 * @return il nome completo del nuovo file
	 */
	private static String creaNomeFileUnione(String nome) {
		
		return ("completo" + Divisione.getSeparatore() + nome);
		
	}
	
	/**
	 * Metodo che restituisce il nome del file passato come parametro senza l'estensione.
	 * 
	 * @param file il file da cui rimuovere l'estensione
	 * @return il nome senza estensione del file 
	 */
	private static String getNomeFileSenzaEstensione(File file) {
		
		String nome = file.getName();
		
		if (nome.lastIndexOf(".") != -1 && nome.lastIndexOf(".") != 0) {
			
			return nome.substring(0, nome.lastIndexOf("."));
		}
		else
			return nome;
		
	}
	
	/**
	 * Metodo che restituisce l'estensione del file passato come parametro. Se l'estensione non � presente,
	 * restituisce una stringa vuota.
	 * 
	 * @param file il file da cui estrarre l'estensione
	 * @return l'estensione del file, altrimenti una stringa vuota
	 */
	private static String getEstensioneFile(File file) {
		
		String nome = file.getName();
		
		if (nome.lastIndexOf(".") != -1 && nome.lastIndexOf(".") != 0) {
			
			return nome.substring(nome.lastIndexOf("."));
		}
		else
			return "";
		
	}
	
	/**
	 * Metodo che rinomina il file passato come parametro utilizzando il numero differenziante specificato.
	 * 
	 * @param fileDaRinominare il file da rinominare
	 * @param differenziatore un numero da apporre fra parentesi alla fine del nome del file
	 * @return il file rinominato
	 */
	private static File rinominaFile(File fileDaRinominare, int differenziatore) {
		
		String nomeSenzaEsten = getNomeFileSenzaEstensione(fileDaRinominare);
	        	
		File fileNuovoNome = new File(fileDaRinominare.getParentFile(), 
				nomeSenzaEsten.concat("(" + differenziatore + ")" + getEstensioneFile(fileDaRinominare)));
		
		return fileNuovoNome;
		
	}
	
	/**
	 * Metodo che crea fisicamente un file nella cartella passata come parametro utilizzando il nome specificato, 
	 * per poi restituirlo.
	 * <p>
	 * Se il file esiste gi�, esso viene rinominato invocando {@link #rinominaFile(File, int)}.
	 * 
	 * @param cartella la cartella in cui dovr� essere creato il file
	 * @param nomeNuovoFile il nome che il nuovo file dovr� avere
	 * @return il file creato
	 */
	private static File creaFileFisico (File cartella, String nomeNuovoFile) {
		
		File nuovoFile = new File(cartella, nomeNuovoFile);

		nuovoFile.getParentFile().mkdirs();
			
		int differenziatore = 1;
		
		while (nuovoFile.exists()) {
			
			nuovoFile = rinominaFile(nuovoFile, differenziatore);
			
			JOptionPane.showMessageDialog(null, 
					"<html><body><p style='width: 500px;'>Il file " + nomeNuovoFile + " � stato rinominato in " + 
					nuovoFile.getName() + ".</p></body></html>", 
					"File rinominato", JOptionPane.INFORMATION_MESSAGE);
			
			differenziatore++;
		}
			
		try {
				
			nuovoFile.createNewFile();
		}
		catch (IOException ioe) {
			
			JOptionPane.showMessageDialog(null, 
					"<html><body><p style='width: 500px;'>Non � stato possibile creare il file " + 
					nuovoFile.getName() + ".</p></body></html>", 
					"Errore", JOptionPane.ERROR_MESSAGE);
		}	
		
		return nuovoFile;	
	}

}
