package FileSplitter;

/**
 * Interfaccia utilizzabile da una classe che effettua la {@link Divisione} di un file in base
 * al numero di parti in cui dovr� essere diviso.
 * 
 * @author Laura Morselli
 *
 */
public interface DivisionePerParti {

	/**
	 * Metodo per calcolare la dimensione che ognuno dei file generati dalla divisione dovr� avere,
	 * conoscendo il numero di parti in cui il file originale dovr� essere diviso.
	 * 
	 * @param numParti il numero di parti in cui un file sar� diviso
	 * @return la dimensione che ognuno dei file generati dalla divisione dovr� avere
	 */
	public double calcolaDimParte(int numParti);
}
