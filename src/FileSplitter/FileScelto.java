package FileSplitter;

import java.io.*;
import FileSplitter.Divisione.tipoDivisione;

/** 
 * Classe che descrive un file scelto per la {@link Divisione}.
 * 
 * @author Laura Morselli
 *
 */
public class FileScelto {
	
	/**
	 * Costante che rappresenta 1 kB come 1024 byte.
	 */
	private final int KB = 1024;
	
	/**
	 * L'identificatore numerico del file.
	 */
	private int id;
	/**
	 * Il file scelto per la divisione.
	 */
	private File file;
	/**
	 * Il tipo di divisione da attuare sul {@link #file}.
	 */
	private String tipoDiv;
	
	/**
	 * Crea una nuova istanza di {@link FileScelto} con l'identificativo numerico ed il file specificati.
	 * Il tipo di divisione � impostato a quello di default ({@link tipoDivisione#DIM_PARTI DIM_PARTI}).
	 * 
	 * @param id l'identificativo numerico del file
	 * @param file il file scelto per la divisione
	 */
	public FileScelto (int id, File file) {
		
		this.id = id;
		this.file = file;
		this.tipoDiv = tipoDivisione.DIM_PARTI.getDescDiv() ;
	}
	
	/**
	 * Crea una nuova istanza di {@link FileScelto} con l'identificativo numerico, il file, ed il 
	 * {@link tipoDivisione tipo di divisione} specificati.
	 * 
	 * @param id l'identificativo del file
	 * @param file il file scelto per la divisione
	 * @param tipoDivisione il tipo di divisione
	 */
	public FileScelto (int id, File file, String tipoDivisione) {
		
		this.id = id;
		this.file = file;
		this.tipoDiv = tipoDivisione;
	}
	
	/**
	 * Metodo che restituisce la variabile {@link #id}.
	 * 
	 * @return la variabile {@link #id}
	 */
	public int getID() {
		
		return this.id;
	}
	
	/** 
	 * Metodo che restituisce la variabile {@link #file}.
	 * 
	 * @return la variabile {@link #file}
	 */
	public File getFile() {
		
		return this.file;
	}
	
	/**
	 * Metodo che restituisce la variabile {@link #tipoDiv}.
	 * 
	 * @return la variabile {@link #tipoDiv}
	 */
	public String getTipoDivisione() {
		
		return this.tipoDiv;
	}
	
	/**
	 * Metodo che restituisce una stringa descrivente la dimensione e l'unit� di grandezza (cio� kB, MB, GB)
	 * del file.
	 * 
	 * @return dimensione e unit� di grandezza del file
	 */
	public String getDescDimFile() {
		
		int dimFile = (int) this.file.length();
		
		if (dimFile < KB ) {
			
			return (dimFile + " bytes");
		}
		if (dimFile < KB*KB) {
			
			return (dimFile / KB + " kB");
		}
		if (dimFile < KB*KB*KB ) {
			
			return (dimFile / (KB*KB) + " MB");
		}
		else
			return (dimFile / (KB*KB*KB) + " GB");
	}
	
	/**
	 * Imposta il {@link tipoDivisione tipo di divisione} a quello specificato.
	 * 
	 * @param tipoDivisione il tipo di divisione
	 */
	public void setTipoDivisione(String tipoDivisione) {
		
		this.tipoDiv = tipoDivisione;
	}
	
	/**
	 * Metodo che restituisce una descrizione degli attributi di questa istanza di {@link FileScelto}.
	 * 
	 * @return la descrizione degli attributi di questa istanza di {@link FileScelto}
	 */
	@Override
	public String toString () {
		
		return (this.id + ". " + this.file.getName() + ", "
				+ "tipo: " + this.tipoDiv);
	}

}
