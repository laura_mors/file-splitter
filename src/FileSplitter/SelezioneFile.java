package FileSplitter;

import java.io.*;
import javax.swing.*;
import javax.swing.filechooser.*;

/**
 * Classe che descrive un meccanismo per permettere all'utente di selezionare dei file.
 * 
 * @author Laura Morselli
 *
 */
public class SelezioneFile {
	
	/**
	 * Il {@code JFileChooser} utilizzato.
	 */
	private JFileChooser fc;
	/**
	 * Il valore restituito dall'azione condotta sul {@code JFileChooser} {@link #fc}.
	 */
	private int valoreRitorno;
	/**
	 * L'array contenente il file, o i file, selezionati.
	 */
	private File[] files;
	
	/**
	 * Crea una nuova istanza di {@link SelezioneFile}, la quale punta alla home directory dell'utente.
	 * La modalit� di selezione dei file � impostata ai soli file.
	 */
	public SelezioneFile() {
		
		this.fc = new JFileChooser(FileSystemView.getFileSystemView().getHomeDirectory());
		this.fc.setFileSelectionMode(JFileChooser.FILES_ONLY);
	}
	
	/**
	 * Metodo che permette di effettuare la selezione di pi� file alla volta. Se sono stati scelti dei file
	 * restituisce {@code true}, {@code false} altrimenti.
	 * 
	 * @return {@code true} se sono stati scelti dei file, {@code false} altrimenti 
	 */
	public boolean selezioneMultipla() {
		
		fc.setMultiSelectionEnabled(true);
		
		valoreRitorno = fc.showDialog(null, "Scegli dei file");
		
		if (valoreRitorno == JFileChooser.APPROVE_OPTION) {
			
				files = fc.getSelectedFiles();
				return true;
		}
		else 
			return false;
		
	}
	
	/**
	 * Metodo che permette di effettuare la selezione di un singolo file. Se � stato scelto un file restituisce
	 * {@code true}, {@code false} altrimenti.
	 * 
	 * @return {@code true} se � stato scelto un file, {@code false} altrimenti
	 */
	public boolean selezioneSingola() {
		
		valoreRitorno = fc.showDialog(null, "Scegli un file");
		
		if (valoreRitorno == JFileChooser.APPROVE_OPTION) {
			
			files = new File[1];
			files[0] = fc.getSelectedFile();
			return true;
		}
		else 
			return false;
		
	}
	
	/**
	 * Metodo che restituisce la variabile {@link #files}.
	 * 
	 * @return la variabile {@link #files}
	 */
	public File[] getFileSelezionati() {
		
		return this.files;
	}
	

}
